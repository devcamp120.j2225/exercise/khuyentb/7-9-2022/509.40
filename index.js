//import thu vien express js 
const express = require('express');

//import thu vien mongoosejs
const mongoose = require("mongoose");

//Khoi tao app Express 
const app = express();

//import path
const path = require('path');

//Khai bao cong chay project
const port = 8000;

//Import router Module
const courseRouter = require("./app/router/courseRouter");

//Import model module
const courseModel = require("./app/model/courseModel");

app.use(express.json());

app.use((req,res,next) => {
    let today = new Date();

    console.log("Current: ", today);

    next();
})

app.use((req,res,next) =>{
    console.log("Methos: ", req.method);
    
    next();
})

mongoose.connect("mongodb://localhost:27017/CRUD_Course365", (error) => {
    if (error) throw error;
    console.log("connect successfully!")
});


// Thêm middleware static để hiển thị ảnh
app.use(express.static(path.join(__dirname, '/views')));

// Đường dẫn tới link
app.get('/', (req, res) => {
     console.log(__dirname);
     res.sendFile(path.join(__dirname, '/views/index.html'));
})


app.listen(port, () => {
    console.log('App listening on port', port);
});

app.use(courseRouter);


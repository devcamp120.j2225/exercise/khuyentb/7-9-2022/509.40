//Import thu vien mongoose
const mongoose = require("mongoose");

//Khai bao class Schema cua thu vien mongoose
const Schema = mongoose.Schema;

//Khai bao ourse schema 
const courseModel = new Schema({
    courseCode: {
         type: String,
         unique: true,
         required: true
    },
    courseName: {
         type: String,
         required: true
    },
    price: {
         type: Number,
         required: true
    },
    discountPrice: {
         type: Number,
         required: true
    },
    duration: {
         type: String,
         required: true
    },
    level: {
         type: String,
         required: true
    },
    coverImage: {
         type: String,
         required: true
    },
    teacherName: {
         type: String,
         required: true
    },
    teacherPhoto: {
         type: String,
         required: true
    },
    isPopular: {
         type: Boolean,
         required: true
    },
    isTrending: {
         type: Boolean,
         required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('course', courseModel);

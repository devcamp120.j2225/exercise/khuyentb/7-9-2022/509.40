const express = require("express");

const router = express.Router();

const {createCourse, getAllCourses, getCourseByID, updateCourse, deleteCourse} = require ("../controller/courseControllers")

router.get("/courses", getAllCourses);

router.get('/courses/:courseId', getCourseByID);

router.post("/courses", createCourse);

router.put('/courses/:courseId', updateCourse);

router.delete('/courses/:courseId', deleteCourse);

module.exports = router;
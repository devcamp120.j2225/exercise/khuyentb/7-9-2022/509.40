// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Course Model
const courseModel = require("../model/courseModel");

// Get all courses
const getAllCourses = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    courseModel.find((error, data) => {
         if (error) {
              return res.status(500).json({
                   message: error.message
              })
         }
         return res.status(200).json({
              message: 'Get all courses',
              courses: data
         })
    })

}

// Get course by ID
const getCourseByID = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let courseID = req.params.courseID;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseID)) {
         return res.status(400).json({
              message: 'Course ID is invalid!'
         })
    }
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    courseModel.findById(courseID, (error, data) => {
         if (error) {
              return res.status(500).json({
                   message: error.message
              })
         }
         return res.status(200).json({
              message: 'Get a course by ID',
              course: data
         })
    })
}

// Create a course
const createCourse = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let body = req.body;
    // B2: Validate dữ liệu
    if (!body.courseCode) {
         return res.status(400).json({
              message: 'Course Code is required!'
         })
    }
    if (!body.courseName) {
         return res.status(400).json({
              message: 'Course Name is required!'
         })
    }
    if (!Number.isInteger(body.price) || body.price < 0) {
         return res.status(400).json({
              message: 'Price is invalid!'
         })
    }
    if (!Number.isInteger(body.discountPrice) || body.discountPrice < 0) {
         return res.status(400).json({
              message: 'Discount Price is invalid!'
         })
    }
    if (!body.duration) {
         return res.status(400).json({
              message: 'Duration is required!'
         })
    }
    if (!body.level) {
         return res.status(400).json({
              message: 'Level is required!'
         })
    }
    if (!body.coverImage) {
         return res.status(400).json({
              message: 'Cover Image is required!'
         })
    }
    if (!body.teacherName) {
         return res.status(400).json({
              message: 'Teacher Name is required!'
         })
    }
    if (!body.teacherPhoto) {
         return res.status(400).json({
              message: 'Teacher Photo is required!'
         })
    }
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newCourse = {
         _id: mongoose.Types.ObjectId(),
         courseCode: body.courseCode,
         courseName: body.courseName,
         price: body.price,
         discountPrice: body.discountPrice,
         duration: body.duration,
         level: body.level,
         coverImage: body.coverImage,
         teacherName: body.teacherName,
         teacherPhoto: body.teacherPhoto,
         isPopular: body.isPopular,
         isTrending: body.isTrending
    }
    courseModel.create(newCourse, (error, data) => {
         if (error) {
              return res.status(500).json({
                   message: error.message
              })
         }
         return res.status(201).json({
              message: 'Create successfully',
              course: data
         })
    })
}

// Update a course
const updateCourse = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let courseID = req.params.courseID;
    let body = req.body;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseID)) {
         return res.status(400).json({
              message: 'Course ID is invalid!'
         })
    }
    if (body.courseCode !== undefined && body.courseCode == '') {
         return res.status(400).json({
              message: 'Course Code is required!'
         })
    }
    if (body.courseName !== undefined && body.courseName == '') {
         return res.status(400).json({
              message: 'Course Name is required!'
         })
    }
    if (body.price !== undefined && !Number.isInteger(body.price) || body.price < 0) {
         return res.status(400).json({
              message: 'Price is invalid!'
         })
    }
    if (body.discountPrice !== undefined && !Number.isInteger(body.discountPrice) || body.discountPrice < 0) {
         return res.status(400).json({
              message: 'Discount Price is invalid!'
         })
    }
    if (body.duration !== undefined && body.duration == '') {
         return res.status(400).json({
              message: 'Duration is required!'
         })
    }
    if (body.level !== undefined && body.level == '') {
         return res.status(400).json({
              message: 'Level is required!'
         })
    }
    if (body.coverImage !== undefined && body.coverImage == '') {
         return res.status(400).json({
              message: 'Cover Image is required!'
         })
    }
    if (body.teacherName !== undefined && body.teacherName == '') {
         return res.status(400).json({
              message: 'Teacher Name is required!'
         })
    }
    if (body.teacherPhoto !== undefined && body.teacherPhoto == '') {
         return res.status(400).json({
              message: 'Teacher Photo is required!'
         })
    }
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let courseUpdate = {
         courseCode: body.courseCode,
         courseName: body.courseName,
         price: body.price,
         discountPrice: body.discountPrice,
         duration: body.duration,
         level: body.level,
         coverImage: body.coverImage,
         teacherName: body.teacherName,
         teacherPhoto: body.teacherPhoto,
         isPopular: body.isPopular,
         isTrending: body.isTrending
    }
    courseModel.findByIdAndUpdate(courseID, courseUpdate, (error, data) => {
         if (error) {
              return res.status(500).json({
                   message: error.message
              })
         }
         return res.status(200).json({
              message: 'Update successfully',
              course: data
         })
    })
}

// Delete a course
const deleteCourse = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let courseID = req.params.courseID;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseID)) {
         return res.status(400).json({
              message: 'Course ID is invalid!'
         })
    }
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    courseModel.findByIdAndDelete(courseID, (error, data) => {
         if (error) {
              return res.status(500).json({
                   message: error.message
              })
         }
         return res.status(204).json({
              message: 'Delete successfully'
         })
    })
}



// Export Course controller thành 1 module
module.exports = {
    createCourse,
    getAllCourses,
    getCourseByID,
    updateCourse,
    deleteCourse
}



